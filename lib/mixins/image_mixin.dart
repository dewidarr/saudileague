

import 'package:flutter/material.dart';

mixin ImageShapeMixin{

  Widget loadCircleImage(String image){
    return Padding(
      padding:  EdgeInsets.only(right:8.0),
      child: Container(
          width: 80.0,
          height: 80.0,
          decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                  fit: BoxFit.fill,
                  image:  NetworkImage(
                      image)
              )
          )),
    );

  }

  Widget loadDecorationImage({@required image,@required logo,@required width,@required height,@required logowidth,@required logoHeight}) {
      return Container(
        width: width,
        height: height,
        decoration:  BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            image:  DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(image)
            )
        ),
        child:  Stack(children: <Widget>[
           Positioned(
            bottom: 0.0,
            right: 0.0,
              child: Image.asset(logo,width: logowidth,height: logoHeight,))
        ],),
      );

  }

}