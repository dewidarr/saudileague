
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_task/screens/media_center/news/news_page.dart';
import 'package:flutter_task/screens/taps_handler.dart';


import 'localization/applocaliz.dart';

void main()=> runApp(SaudiNews());

class SaudiNews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
    debugShowCheckedModeBanner: false,
      supportedLocales: [
        Locale('en', 'US'),
        Locale('ar', ''),
      ],
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      initialRoute: TapsHandler.id,
      routes: {
      TapsHandler.id:(context)=> TapsHandler(),
        NewsPage.id:(context)=> NewsPage(),
      },
    );
  }
}
