import 'package:flutter/material.dart';

class NextMatchesData{
  NextMatchesData({@required this.name,@required this.image,@required this.time,@required this.date});
final String name;
final String image;
final String date;
final String time;
}