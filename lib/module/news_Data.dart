import 'package:flutter/material.dart';

class NewsData {
  NewsData({
    @required this.title,
    @required this.image,
    @required this.fullTxt,
    @required this.date,
    this.article
  });

  final String title;
  final String image;
  final String fullTxt;
  final String date;
  final String article;
}
