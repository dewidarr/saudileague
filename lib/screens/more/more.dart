import 'package:flutter/material.dart';
import 'package:flutter_task/localization/applocaliz.dart';

class More extends StatefulWidget {
  static String id = 'more';

  @override
  _MoreState createState() => _MoreState();
}

class _MoreState extends State<More> {
  @override
  Widget build(BuildContext context) {
    TextStyle _style =
        Theme.of(context).textTheme.title.copyWith(color: Color(0xffa0a3ad));
    return Scaffold(
      backgroundColor: Color(0xff0f1737),
      body: SingleChildScrollView(
        child: categories(context, _style),
      ),
    );
  }

  Padding categories(BuildContext context, TextStyle _style) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 60.0, right: 20.0, bottom: 30.0),
            child: Image.asset(
              'images/5TRrpRAGc.png',
              width: 60.0,
              height: 60.0,
              fit: BoxFit.cover,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              AppLocalizations.of(context).translate('club_dir'),
              style: _style,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              AppLocalizations.of(context).translate('play_guid'),
              style: _style,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              AppLocalizations.of(context).translate('who_are'),
              style: _style,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              AppLocalizations.of(context).translate('laws'),
              style: _style,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              AppLocalizations.of(context).translate('committees'),
              style: _style,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              AppLocalizations.of(context).translate('call'),
              style: _style,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              AppLocalizations.of(context).translate('share'),
              style: _style,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              AppLocalizations.of(context).translate('subscribe'),
              style: _style,
            ),
          ),
          SocialIcons('images/Icon.png'),
          SocialIcons('images/Icon2.png'),
          SocialIcons('images/Icon3.png'),
          SocialIcons('images/Icon5.png'),
          SocialIcons('images/Icon6.png'),
        ],
      ),
    );
  }

  Widget SocialIcons(String image) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[Image.asset(image)],
      ),
    );
  }
}
