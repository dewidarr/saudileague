import 'package:flutter/material.dart';
import 'package:flutter_task/module/news_Data.dart';
import 'package:flutter_task/module/next_matches.dart';
import 'package:flutter_task/screens/home/latest_news/latest_news.dart';
import 'package:flutter_task/screens/home/latest_tweet/latest_tweets.dart';
import 'package:flutter_task/screens/home/next_matches/next_matches.dart';
import 'package:flutter_task/screens/home/predict_winner/predict_winner.dart';
import 'package:flutter_task/screens/home/sponsors.dart';
import 'package:flutter_task/screens/home/videos/home_videos.dart';

import '../../constants.dart';


class Home extends StatefulWidget {
  static String id = 'home';

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Widget> homeViewsList = [];

  @override
  void initState() {
    super.initState();
    homeViewsList.add(LatestNews(newsData:getData()));
    homeViewsList.add(NextMatches(nextMatches: getNextMatche(),));
    homeViewsList.add(LatestTweets());
    homeViewsList.add(PredictWinner());
    homeViewsList.add(HomeVideos());
    homeViewsList.add(Sponsors());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff3f3f3),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Color(0xff0f1737),
            flexibleSpace: FlexibleSpaceBar(
              background: Image.asset(
                'images/Group544.png',
                fit: BoxFit.cover,
              ),
            ),
            expandedHeight: 150.0,
          ),
          SliverList(delegate: SliverChildListDelegate(homeViewsList),)
        ],
      ),
    );
  }

 NewsData getData() {
    return NewsData( title: KNewsTitle,
        image: 'images/img.png',
        fullTxt: KNewsFullText,
        date: KNewsDate);
  }

 NextMatchesData getNextMatche(){
     return  NextMatchesData(name: 'الاهلي',image:'images/clublogo.png' ,time: '22:00',date:'الخميس 15 يوليو' );

  }
}
