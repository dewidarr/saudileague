import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_task/constants.dart';
import 'package:flutter_task/localization/applocaliz.dart';

class LatestTweets extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top:16.0,bottom: 4.0,right: 8.0,left: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).translate('latest_tweets'),
                style: Theme.of(context).textTheme.body2,
              ),
              Text(
                AppLocalizations.of(context).translate('more'),
                style: Theme.of(context)
                    .textTheme
                    .body2
                    .copyWith(color: Colors.lightBlue),
              )
            ],
          ),
        ),
        getTweet(context),
        getTweet(context)
      ],
    );
  }

  Widget getTweet(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Image.asset('images/5TRrpRAGc.png'),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      KNewsTitle,
                      style: Theme.of(context).textTheme.title,
                    ),
                    Text(
                      'account@',
                      style: Theme.of(context).textTheme.caption,
                    )
                  ],
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              KNewsArticle,
            ),
          ),Container(width: double.infinity,height: 0.5,color: Colors.black12,)
        ],
      ),
    );
  }
}
