import 'package:flutter/material.dart';
import 'package:flutter_task/localization/applocaliz.dart';

class HomeVideos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(top:16.0,bottom: 4.0,right: 8.0,left: 8.0),
        child: Row(children: <Widget>[
          Text(AppLocalizations.of(context).translate('videos')),
        ],),
      ),
      Material(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical:20.0,horizontal: 8.0),
          child: Container(
            width: double.infinity,
            height: 150.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('images/Image.png')
              )
            ),
          ),
        ),
      )

    ],);
  }
}
