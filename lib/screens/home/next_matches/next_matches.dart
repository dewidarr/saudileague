import 'package:flutter/material.dart';
import 'package:flutter_task/localization/applocaliz.dart';
import 'package:flutter_task/module/next_matches.dart';

class NextMatches extends StatelessWidget {
  NextMatches({@required this.nextMatches});

  final NextMatchesData nextMatches;

  @override
  Widget build(BuildContext context) {
    TextStyle _style = Theme.of(context).textTheme.body2;
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).translate('next_matches'),
                style: _style,
              ),
              Text(
                AppLocalizations.of(context).translate('more'),
                style: _style.copyWith(color: Colors.lightBlue),
              )
            ],
          ),
        ),
        matches(context),
        matches(context),
        matches(context),
      ],
    );
  }

  Widget matches(BuildContext context) {
     return Material(
       color: Colors.white,
       child: Padding(
         padding: const EdgeInsets.all(10.0),
         child: Container(
           decoration: BoxDecoration(
               border: Border(bottom: BorderSide(color: Colors.black12,width: .5 ))),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
               Row(
                 children: <Widget>[
                   Image.asset(nextMatches.image), Text(nextMatches.name)
                 ],
               ),
               Column(
                 children: <Widget>[
                   Text(nextMatches.time), Text(nextMatches.date,style: Theme.of(context).textTheme.caption,)

                 ],
               ),
               Row(
                 children: <Widget>[
                   Text(nextMatches.name),
                   Image.asset(nextMatches.image)
                 ],
               ),
             ],
           ),
         ),
       ),
     );

  }
}
