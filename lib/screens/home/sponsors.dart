import 'package:flutter/material.dart';
import 'package:flutter_task/localization/applocaliz.dart';
class Sponsors extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(top:16.0,bottom: 4.0,right: 8.0,left: 8.0),
        child: Row(children: <Widget>[
          Text(AppLocalizations.of(context).translate('sponsors'))
        ],),
      ), Padding(
        padding: const EdgeInsets.only(bottom:30.0),
        child: Material(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical:20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
              Image.asset('images/Symbol2_1.png'),
              Image.asset('images/Symbol2_1.png'),
            ],),
          ),
        ),
      )
    ],);
  }
}
