import 'package:flutter/material.dart';
import 'package:flutter_task/localization/applocaliz.dart';
class PredictWinner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(top:16.0,bottom: 4.0,right: 8.0,left: 8.0),
        child: Row(
          children: <Widget>[
            Text(AppLocalizations.of(context).translate('predict_winner'),style:Theme.of(context).textTheme.body2 ,),

        ],),
      ),
      Material(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
            Winners('الاتحاد','30%'),
            Winners('الهلال','50%'),
            Winners('النهضة','20%'),
          ],),
        ),
      )

    ],);
  }

}

class Winners extends StatelessWidget {
  Winners(this.name,this.percent);
  final String name;
  final String percent;
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Container(
          width: 80.0,
          height: 60.0,
          padding: const EdgeInsets.all(10.0),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black12),
              borderRadius: BorderRadius.all(Radius.circular(5.0))
          ),
          child: Image.asset('images/clublogo.png')),
      Padding(
        padding: const EdgeInsets.only(top:8.0),
        child: Text(name,style: Theme.of(context).textTheme.body2,),
      ),
      Text(percent,style: Theme.of(context).textTheme.caption,)
    ],);
  }
}

