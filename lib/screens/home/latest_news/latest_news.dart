import 'package:flutter/material.dart';
import 'package:flutter_task/localization/applocaliz.dart';
import 'package:flutter_task/mixins/image_mixin.dart';
import 'package:flutter_task/module/news_Data.dart';

class LatestNews extends StatelessWidget with ImageShapeMixin{
  LatestNews({@required this.newsData});
  final NewsData newsData;
  @override
  Widget build(BuildContext context) {
    TextStyle _style = Theme.of(context).textTheme.body2;
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom:8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
            Text(AppLocalizations.of(context).translate('latest_news'),style: _style,),
            Text(AppLocalizations.of(context).translate('more'),style: _style.copyWith(color: Colors.lightBlue),)
          ],),
        ),
          loadDecorationImage(image: newsData.image, logo: 'images/Group545.png', width: double.infinity, height: 180.0, logowidth: 50.0, logoHeight: 50.0),
          Text(newsData.title,style: Theme.of(context).textTheme.caption,),
          Text(newsData.fullTxt,style: Theme.of(context).textTheme.subtitle,)
      ],),
    );
  }
}
