import 'package:flutter/material.dart';
import 'package:flutter_task/localization/applocaliz.dart';
import 'package:flutter_task/screens/league_table/league_record_view.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'league_data.dart';

final leagueUrl = 'https://reqres.in/api/users';

class LeagueTable extends StatefulWidget {
  static String id = 'league_table';

  @override
  _LeagueTableState createState() => _LeagueTableState();
}

class _LeagueTableState extends State<LeagueTable> {
  bool _isLoading = false;
  List<LeagueRecordView> _leagueList = [];

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: CustomScrollView(
        slivers: <Widget>[
          SliverPersistentHeader(
            delegate: MySliverAppBar(expandedHeight: 150),
            pinned: true,
          ),_isLoading? SliverFillRemaining(child:  Center(child: CircularProgressIndicator()),):
         SliverList(
           delegate: SliverChildListDelegate(_leagueList),
         )
        ],
      ),
    );
  }

  void getData() async {
    _isLoading = true;

    final List<LeagueRecordView> leagueList =[];
    http.Response response = await http.get(leagueUrl);
    if (response.statusCode == 200) {
     var decodeData = jsonDecode(response.body);
     List<dynamic> data = decodeData['data'];
  if(data == null){
    setState(() {
      _isLoading = false;
    });
  }
     for(var leagueRecord=0 ; leagueRecord < data.length ; leagueRecord++){
       LeagueData leagueData = LeagueData(
         id: data[leagueRecord]['id'],
         email: data[leagueRecord]['email'],
         first_name: data[leagueRecord]['first_name'],
         last_name: data[leagueRecord]['last_name'],
         avatar: data[leagueRecord]['avatar'],
       );
       leagueList.add(LeagueRecordView(leagueData: leagueData,));
     }

     setState(() {
       _leagueList = leagueList;
       _isLoading = false;
       print('isloading'+ _isLoading.toString());
     });
     print(data);
    }
  }

 Widget showList() {
    return ListView(
      children: _leagueList,
    );
 }
}


class MySliverAppBar extends SliverPersistentHeaderDelegate {
  final double expandedHeight;

  MySliverAppBar({@required this.expandedHeight});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        Image.asset(
          'images/Group5442.png',
          fit: BoxFit.cover,
        ),
        Center(
          child: Opacity(
            opacity: shrinkOffset / expandedHeight,
            child: Text(
              AppLocalizations.of(context).translate('league_table'),
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontSize: 23,
              ),
            ),
          ),
        ),
        Positioned(
          top: expandedHeight / 2 - shrinkOffset,
          left: MediaQuery.of(context).size.width / 3,
          child: Opacity(
            opacity: (1 - shrinkOffset / expandedHeight),
            child: Card(
              color: Color(0xff0f1737),
              elevation: 10,
              child: Image.asset('images/5TRrpRAGc.png',)
            ),
          ),
        ),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}