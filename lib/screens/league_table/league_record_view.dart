
import 'package:flutter/material.dart';
import 'package:flutter_task/mixins/image_mixin.dart';
import 'package:flutter_task/screens/league_table/league_data.dart';
class LeagueRecordView extends StatelessWidget with ImageShapeMixin {
  LeagueRecordView({@required this.leagueData});
  final LeagueData leagueData;
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 2.0,
      borderRadius: BorderRadius.only(topLeft:Radius.circular(4.0),bottomLeft:Radius.circular(4.0) ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: loadCircleImage(leagueData.avatar),
              ),
              Padding(
                padding: const EdgeInsets.only(top:12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                  Text(leagueData.first_name + leagueData.last_name,style: Theme.of(context).textTheme.title,),
                  Text(leagueData.email,style: Theme.of(context).textTheme.caption,),
                ],),
              )

            ],
          ),

        ],),
      ),
    );
  }
}
