import 'package:flutter/material.dart';
import 'package:flutter_task/screens/full_stats/full_stats.dart';
import 'package:flutter_task/screens/league_table/league_table.dart';
import 'package:flutter_task/screens/media_center/media_center_handler.dart';
import 'package:flutter_task/screens/more/more.dart';

import '../localization/applocaliz.dart';
import 'home/home.dart';

class TapsHandler extends StatefulWidget {
  static String id = 'taps_handler';

  @override
  _TapsHandlerState createState() => _TapsHandlerState();
}

class _TapsHandlerState extends State<TapsHandler> {
  int _selectedIndex = 0;
  final List<Widget> _tabsActionList = [
   Home(),
   LeagueTable(),
   MediaCenter(),
   FullStats(),
   More(),
  ];

  void _onItemTaped(int index){
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: _tabsActionList.elementAt(_selectedIndex)),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Color(0xff0f1737)
        ),
        child: bottomNavigation(context),
      ),
    );
  }

  BottomNavigationBar bottomNavigation(BuildContext context) {
    return BottomNavigationBar(
        selectedLabelStyle: TextStyle(color: Colors.white),
        unselectedLabelStyle: TextStyle(color: Colors.grey),
        showUnselectedLabels: true,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            activeIcon: Image.asset('images/Glyph.png',color: Colors.white,),
            icon: Image.asset('images/Glyph.png',color: Colors.grey,),
            title: Text(AppLocalizations.of(context).translate('home')),
          ),
          BottomNavigationBarItem(
            activeIcon: Image.asset('images/Glyph2.png',color: Colors.white,),
            icon: Image.asset('images/Glyph2.png'),
            title: Text(AppLocalizations.of(context).translate('league_table')),
          ),
          BottomNavigationBarItem(
            activeIcon: Image.asset('images/Glyph3.png',color: Colors.white,),
            icon: Image.asset('images/Glyph3.png'),
            title: Text(AppLocalizations.of(context).translate('media_center')),
          ),
          BottomNavigationBarItem(
            activeIcon: Image.asset('images/Glyph4.png',color: Colors.white,),
            icon:Image.asset('images/Glyph4.png'),
            title: Text(AppLocalizations.of(context).translate('full_stats')),
          ),
          BottomNavigationBarItem(
            activeIcon: Image.asset('images/Glyph5.png',color: Colors.white,),
            icon: Image.asset('images/Glyph5.png'),
            title: Text(AppLocalizations.of(context).translate('more')),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        onTap: _onItemTaped,
      );
  }
}
