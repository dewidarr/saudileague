import 'package:flutter/material.dart';
import 'package:flutter_task/mixins/image_mixin.dart';
import 'package:flutter_task/module/news_Data.dart';
import 'package:flutter_task/screens/media_center/news/news_page.dart';

class ArticleView extends StatelessWidget with ImageShapeMixin {
  ArticleView({@required this.newsData});

  final NewsData newsData;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(context,MaterialPageRoute(builder: (context)=> NewsPage(newsData: newsData,)));
        },
        child: Material(
          color: Color(0xfff3f3f3),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: loadDecorationImage(
                    image: newsData.image, logo: 'images/Group545.png',
                  width: 120.0,height: 100.0,logowidth: 30.0,logoHeight: 30.0
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      newsData.title,
                      style: Theme.of(context).textTheme.body1,
                    ),
                    Text(
                      newsData.fullTxt,
                      style: Theme.of(context).textTheme.body2,
                    ),
                    Text(
                      newsData.date,
                      style: Theme.of(context).textTheme.caption,
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
