import 'package:flutter/material.dart';
import 'package:flutter_task/constants.dart';
import 'package:flutter_task/module/news_Data.dart';
import 'package:flutter_task/screens/media_center/news/single_record_view.dart';


class MediaNews extends StatefulWidget {
  @override
  _MediaNewsState createState() => _MediaNewsState();
}

class _MediaNewsState extends State<MediaNews> {
  List<ArticleView> newsList = [];

  @override
  void initState() {
    super.initState();
    newsList.add(ArticleView(
      newsData: getData('images/img.png'),
    ));newsList.add(ArticleView(
      newsData: getData('images/img2.png'),
    ));newsList.add(ArticleView(
      newsData: getData('images/img3.png'),
    ));newsList.add(ArticleView(
      newsData: getData('images/img2.png'),
    ));
    newsList.add(ArticleView(
      newsData: getData('images/img3.png'),
    ));
    newsList.add(ArticleView(
      newsData: getData('images/img.png'),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Color(0xfff3f3f3),
      child: ListView(
        children: newsList,
      ),
    );
  }
}


NewsData getData(String image){
 return NewsData(
      title: KNewsTitle,
      image: image,
      fullTxt: KNewsFullText,
      date: KNewsDate);
}