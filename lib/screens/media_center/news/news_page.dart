import 'package:flutter/material.dart';
import 'package:flutter_task/constants.dart';
import 'package:flutter_task/module/news_Data.dart';

const _imageHeight = 250.0;

class NewsPage extends StatefulWidget {
  static String id = 'news_page';

  NewsPage({@required this.newsData});

  final NewsData newsData;

  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Color(0xff0f1737),
            flexibleSpace: collapsedImage(),
            expandedHeight: _imageHeight,
          ),
          SliverList(delegate: SliverChildListDelegate(showData(context)),)
        ],
      ),
    );
  }

  Widget collapsedImage() {
    return FlexibleSpaceBar(
      background: Container(
        height: _imageHeight,
        decoration: BoxDecoration(
            image:  DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(widget.newsData.image)
            )
        ),
        child: Stack(
          children: <Widget>[
            Positioned(
              left: 10.0,
              top: 40.0,
              child: Image.asset('images/Fill67.png'),
            )
          ],
        ),
      ),
    );
  }

  List<Widget> showData(BuildContext context) {
    List<Widget> list =[];
    list.add(getArticleView());
    return list;
  }

  Widget getArticleView() {
   return Padding(
     padding: const EdgeInsets.all(8.0),
     child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            widget.newsData.fullTxt,
            style: Theme.of(context).textTheme.title,
          ),
          Text(
            widget.newsData.title,
            style: Theme.of(context)
                .textTheme
                .subhead
                .copyWith(color: Colors.black45),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Text(
              widget.newsData.date,
              style: Theme.of(context).textTheme.caption,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              KNewsArticle +
                  KNewsArticle +
                  KNewsArticle +
                  KNewsArticle +
                  KNewsArticle +
                  KNewsArticle +
                  KNewsArticle,
              style: Theme.of(context).textTheme.body2,
            ),
          ),
        ],
      ),
   );
  }
}
