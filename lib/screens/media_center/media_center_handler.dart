import 'package:flutter/material.dart';
import 'package:flutter_task/localization/applocaliz.dart';
import 'package:flutter_task/screens/media_center/news/media_news.dart';

class MediaCenter extends StatefulWidget {
  static String id = 'media_center';

  @override
  _MediaCenterState createState() => _MediaCenterState();
}

class _MediaCenterState extends State<MediaCenter>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Color(0xff0f1737),
            pinned: true,
            snap: true,
            floating: true,
            expandedHeight: 150.0,
            flexibleSpace: FlexibleSpaceBar(
              titlePadding: EdgeInsets.only(bottom: 40.0),
              title: Image.asset('images/5TRrpRAGc.png'),
              centerTitle: true,
              background: Image.asset(
                'images/Group5442.png',
                fit: BoxFit.cover,
              ),
            ),
            bottom: TabBar(
              indicatorWeight: 4.0,
              indicatorColor: Color(0xfff3f3f3),
              controller: _tabController,
              tabs: <Widget>[
                Tab(
                  text: AppLocalizations.of(context).translate('news'),
                ),
                Tab(
                  text: AppLocalizations.of(context).translate('images'),
                ),
                Tab(
                  text: AppLocalizations.of(context).translate('videos'),
                ),
              ],
            ),
          ),
          SliverFillRemaining(
            child: TabBarView(
              controller: _tabController,
              children: <Widget>[
               MediaNews(),
                Text("Tab 2"),
                Text("Tab 3"),
              ],
            ),
          ),


        ],
      ),
    );
  }
}
