import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:marvel/models/character_data.dart';

class CharacterView extends StatelessWidget {
  CharacterView({@required this.characterData,@required this.thumbnail});
  final CharacterData characterData;
  final Thumbnail thumbnail;
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: thumbnail.path +'.'+ thumbnail.extension,
      imageBuilder: (context, imageProvider) => Container(
        width: double.infinity,
        height: 150.0,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget>[
            Positioned(
              bottom: 16.0,
              left: 12.0,
              child: characterName(context),
            )
          ],
        ),
      ),
      placeholder: (context, url) => Center(child: CircularProgressIndicator()),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  Widget characterName(BuildContext context) => Material(
    color: Colors.white,
    child: SingleChildScrollView(
      child: Container(
        width: 120.0,
        height: 40.0,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle
        ),
        child: Center(child: Text(characterData.name,style: Theme.of(context).textTheme.title,)),

      ),
    ),
  );
}
