
import 'character_item_view.dart';

abstract class ICharacterContract{
  void addRecord(List<CharacterView>characterView);
  void showLoading();
  void hideLoading();
}