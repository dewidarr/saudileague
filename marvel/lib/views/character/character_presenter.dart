import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:http/http.dart' as http;
import 'package:marvel/models/character_data.dart';

import '../../consts.dart';
import 'character_item_view.dart';
import 'common_char_contract.dart';

const url = 'https://gateway.marvel.com:443/v1/public/characters';

class CharacterPresenter {
  String _hashKey ;
  final _itemsLimit = 15;
  int _page = 0;
  int _offset = 0;

  final ICharacterContract _iCharacterContract;

  CharacterPresenter(this._iCharacterContract);

  void setUp() async {
    _offset = _page*_itemsLimit;
    _hashKey = generateMd5(KMd5Data);
    _iCharacterContract.showLoading();
      http.Response response = await http.get(getQuery());
      final List<CharacterView> characterList = [];
      if (response.statusCode == 200) {
        var decodeData = jsonDecode(response.body);
        List<dynamic> data = decodeData['data']['results'];
        if (data == null) {
          _iCharacterContract.hideLoading();
        }

        for (var chaRecord = 0; chaRecord < data.length; chaRecord++) {
          CharacterData characterData = CharacterData.fromJson(data[chaRecord]);
          Thumbnail thumbnail = Thumbnail.fromJson(data[chaRecord]['thumbnail']);

          characterList.add(CharacterView(
            characterData: characterData,
            thumbnail: thumbnail,
          ));
        }
        _iCharacterContract.addRecord(characterList);
        _iCharacterContract.hideLoading();
        _page++;
      }

  }

  generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  String getQuery() {
    return 'https://gateway.marvel.com:443/v1/public/characters?limit=$_itemsLimit&offset=$_offset&ts=$Kts&apikey=$KPublic_Key&hash=$_hashKey';
  }
}
