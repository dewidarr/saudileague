import 'package:flutter/material.dart';
import 'package:marvel/views/character/character_item_view.dart';
import 'package:marvel/views/character/character_presenter.dart';
import 'package:marvel/views/character/common_char_contract.dart';



class HomeHandler extends StatefulWidget {
  static String id = 'homeHandelr';

  @override
  _HomeHandlerState createState() => _HomeHandlerState();
}

class _HomeHandlerState extends State<HomeHandler> implements ICharacterContract{
  bool _isLoading = false;
  List<CharacterView> _characterList = [];
  CharacterPresenter _characterPresenter;

  ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    _characterPresenter = CharacterPresenter(this);
    _characterPresenter.setUp();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff282c30),
      body: Center(child: _isLoading ? Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
        CircularProgressIndicator(backgroundColor: Color(0xff282c30),)
      ],):
      ListView.builder(
        controller: _controller,
        itemCount: _characterList.length,
          itemBuilder: (_,index){
       return _characterList[index];
      })
      ),
    );
  }

  @override
  void addRecord(List<CharacterView> characterView) {
    setState(() {
   this._characterList.addAll(characterView);
    });
  }

  @override
  void hideLoading() {
    setState(() {
      this._isLoading = false;
    });
  }

  @override
  void showLoading() {
   setState(() {
     this._isLoading = true;
   });
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
     _characterPresenter.setUp();
    }

  }

}

