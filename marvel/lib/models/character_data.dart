import 'package:flutter/material.dart';

class CharacterData{
  CharacterData({@required this.id,@required this.name,@required this.description,@required this.resourceURI});

  int id;
  String name;
  String description;
  String resourceURI;

  CharacterData.fromJson(Map<String,dynamic> json){
    id = json['id'];
    name = json['name'];
    description = json['description'];
    resourceURI = json['resourceURI'];
  }
}

class Thumbnail{
  Thumbnail({@required this.path,@required this.extension});
   String path;
   String extension;

  Thumbnail.fromJson(Map<String, dynamic> json) {
    path = json['path'];
    extension = json['extension'];
  }

}